<?php

/**
 * @file
 * Front end pages for Autolink Tracking module.
 */

/**
 * Callback function to build the related links page.
 */
function autolink_tracking_overview_incoming_links_page($node) {
  return drupal_get_form('autolink_tracking_overview_incoming_links', $node->nid);
}

/**
 * Form builder to list incoming links.
 *
 * @ingroup forms
 * @see autolink_overview_incoming_links_submit()
 * @see theme_autolink_overview_incoming_links()
 */
function autolink_tracking_overview_incoming_links(&$form_state, $nid) {
  $groups = autolink_get_groups();
  $form = array('#tree' => TRUE);

  $header = array();
  $header[] = array('data' => t('Link type'), 'field' => 'a.type', 'sort' => 'asc');
  $header[] = array('data' => t('Group'), 'field' => 'a.group_type', 'sort' => 'asc');
  $header[] = array('data' => t('Keyword'), 'field' => 'a.keyword', 'sort' => 'asc');
  $header[] = t('Nodes');
  $header[] = t('Average relevance');

  if (user_access('administer autolink')) {
    $header[] = t('Operations');
  }

  $sql = "SELECT a.lid, a.type, a.group_type, a.keyword, r.nid, r.relevance FROM {autolink} a LEFT JOIN {autolink_relationships} r ON a.lid = r.lid WHERE a.link_path = 'node/". $nid ."'";
  $query_count = db_query("SELECT COUNT(a.lid) FROM {autolink} a WHERE a.link_path = 'node/". $nid ."'";

  $sql .= tablesort_sql($header);
  $result = pager_query($sql, 25, 0, $query_count);

  while ($link = db_fetch_object($result)) {
    $paths = array();
    if (isset($link->nid)) {
      foreach ($link->nid as $node) {
        if (variable_get('autolink_display_alias', 1) == AUTOLINK_SETTING_ENABLED) {
          $alias = drupal_lookup_path('alias', 'node/'. $node);
          $paths[] = $alias ? $alias : 'node/'. $node;
        }
        else {
          $paths[] = 'node/' . $node;
        }
      }
    }

    $relevance = is_array($link->relevance) ? array_sum($link->relevance) / count($link->relevance) : $link->relevance;
    $form[$link->lid]['type'] = array('#value' => check_plain($link->type));
    $form[$link->lid]['group_type'] = array('#value' => check_plain($groups[$link->group_type]->name));
    $form[$link->lid]['keyword'] = array('#value' => check_plain(html_entity_decode($link->keyword)));
    $form[$link->lid]['paths'] = array('#value' => check_plain(implode(', ', $paths)));
    $form[$link->lid]['relevance'] = array('#value' => check_plain($relevance));
    if (user_access('administer autolink')) {
      $form[$link->lid]['edit'] = array('#value' => l(t('edit link'), "admin/content/autolink/$link->group_type/edit/link/$link->lid"));
    }
  }

  return $form;
}

/**
 * Theme the incoming links overview as a sortable list of links.
 *
 * @ingroup themeable
 * @see autolink_overview_incoming_links()
 */
function theme_autolink_tracking_overview_incoming_links($form) {
  $header = array(
    theme('table_select_header_cell'),
    array('data' => t('Link type'), 'field' => 'a.type', 'sort' => 'asc'),
    array('data' => t('Group'), 'field' => 'a.group_type', 'sort' => 'asc'),
    array('data' => t('Keyword'), 'field' => 'a.keyword', 'sort' => 'asc'),
    t('Nodes'),
    t('Average relevance'),
  );

  if (user_access('administer autolink')) {
    $header[] = array('data' => t('Operations'), 'colspan' => '1');
  }

  $rows = array();
  foreach (element_children($form) as $key) {
    if (isset($form[$key]['keyword'])) {
      $link = &$form[$key];

      $row = array();
      $row[] = drupal_render($link['type']);
      $row[] = drupal_render($link['group_type']);
      $row[] = drupal_render($link['keyword']);
      $row[] = drupal_render($link['paths']);
      $row[] = drupal_render($link['relevance']);
      if (user_access('administer autolink')) {
        $row[] = drupal_render($link['edit']);
      }

      $rows[] = $row;
    }
  }

  if (empty($rows)) {
    $rows[] = array(array('data' => t('No links available.'), 'colspan' => '6'));
  }

  $output = theme('table', $header, $rows);
  if ($form['pager']['#value']) {
    $output .= drupal_render($form['pager']);
  }

  $output .= drupal_render($form);

  return $output;
}

/**
 * Callback function to build the related links page.
 */
function autolink_tracking_overview_outgoing_links_page($node) {
  return drupal_get_form('autolink_tracking_overview_outgoing_links', $node->nid);
}

/**
 * Form builder to list outgoing links from a specific node.
 *
 * @ingroup forms
 * @see autolink_overview_outgoing_links_submit()
 * @see theme_autolink_overview_outgoing_links()
 */
function autolink_tracking_overview_outgoing_links(&$form_state, $nid) {
  $groups = autolink_get_groups();
  $form = array('#tree' => TRUE);

  $header = array();
  $header[] = array('data' => t('Type'), 'field' => 'a.type', 'sort' => 'asc');
  $header[] = array('data' => t('Link group'), 'field' => 'a.group_type', 'sort' => 'asc');
  $header[] = array('data' => t('Keyword'), 'field' => 'a.keyword', 'sort' => 'asc');
  $header[] = array('data' => t('Destination'), 'field' => 'a.argument', 'sort' => 'asc');
  $header[] = array('data' => t('Relevance'), 'field' => 'r.relevance', 'sort' => 'asc');

  if (user_access('administer autolink')) {
    $header[] = t('Operations');
  }

  $sql = 'SELECT a.lid, a.type, a.group_type, a.keyword, a.argument, r.relevance FROM {autolink} a LEFT JOIN {autolink_relationships} r ON a.lid = r.lid WHERE r.nid = '. $nid;
  $query_count = 'SELECT COUNT(a.lid) FROM {autolink} a LEFT JOIN {autolink_relationships} r ON a.lid = r.lid WHERE r.nid = '. $nid;

  $sql .= tablesort_sql($header);
  $result = pager_query($sql, 25, 0, $query_count);

  while ($link = db_fetch_object($result)) {
    $form[$link->lid]['type'] = array('#value' => check_plain($link->type));
    $form[$link->lid]['group_type'] = array('#value' => check_plain($groups[$link->group_type]->name));
    $form[$link->lid]['keyword'] = array('#value' => check_plain($link->keyword));
    $form[$link->lid]['destination'] = array('#value' => check_plain(html_entity_decode($link->argument)));
    $form[$link->lid]['relevance'] = array('#value' => check_plain($link->relevance));
    if (user_access('administer autolink')) {
      $form[$link->lid]['edit'] = array('#value' => l(t('edit link'), "admin/content/autolink/$link->group_type/edit/link/$link->lid"));
    }
  }

  return $form;
}

/**
 * Theme the outgoing linkgs overview as a sortable list of links.
 *
 * @ingroup themeable
 * @see autolink_overview_outgoing_links()
 */
function theme_autolink_tracking_overview_outgoing_links($form) {
  $header = array(
    theme('table_select_header_cell'),
    array('data' => t('Type'), 'field' => 'a.type', 'sort' => 'asc'),
    array('data' => t('Link group'), 'field' => 'a.group_type', 'sort' => 'asc'),
    array('data' => t('Keyword'), 'field' => 'a.keyword', 'sort' => 'asc'),
    array('data' => t('Destination'), 'field' => 'a.argument', 'sort' => 'asc'),
    array('data' => t('Relevance'), 'field' => 'r.relevance', 'sort' => 'asc'),
  );

  if (user_access('administer autolink')) {
    $header[] = array('data' => t('Operations'), 'colspan' => '1');
  }

  $rows = array();
  foreach (element_children($form) as $key) {
    if (isset($form[$key]['keyword'])) {
      $link = &$form[$key];

      $row = array();
      $row[] = drupal_render($link['type']);
      $row[] = drupal_render($link['group_type']);
      $row[] = drupal_render($link['keyword']);
      $row[] = drupal_render($link['destination']);
      $row[] = drupal_render($link['relevance']);
      if (user_access('administer autolink')) {
        $row[] = drupal_render($link['edit']);
      }

      $rows[] = $row;
    }
  }

  if (empty($rows)) {
    $rows[] = array(array('data' => t('No links available.'), 'colspan' => '6'));
  }

  $output = theme('table', $header, $rows);
  if ($form['pager']['#value']) {
    $output .= drupal_render($form['pager']);
  }

  $output .= drupal_render($form);

  return $output;
}
