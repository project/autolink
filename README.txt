=======================================================================================
|||||||||||||||||||||||||||||||||||||  AUTOLINK  ||||||||||||||||||||||||||||||||||||||
=======================================================================================

------------------------
      DESCRIPTION
------------------------
Autolink provides an API and interface for allowing administrators to define
keywords to be converted to links on node bodies and teasers. When nodes are
created or saved, Autolink will automatically generate links where it locates
designated keywords. This process in conjuction with bulk operations can vastly
improve navigation across sites and make content more accessible to site users.


------------------------
     INSTALLATION
------------------------
1. Download the Autolink module (http://drupal.org/sandbox/jordojuice/1143784).
2. Download the Simple HTML DOM API (http://drupal.org/project/simplehtmldom).
3. Download the AHAH Helper module (http://drupal.org/project/ahah_helper).
1. Extract the tar.gz of each module into your 'modules' directory: 'sites/all/modules'.
2. Enable the Simple HTML DOM API, AHAH Helper, and desired Autolink modules at
   administer >> site building >> modules'.
   Note: In itself the Autolink API does not provide an interface for creating link
         definitions or generating links. You must enable one of the addon modules
     (Autolink Links, Autolink Search, Autolink Taxonomy) to enable the definition
     of link generation rules. See below for details on addon modules.


------------------------
     CONFIGURATION
------------------------
1. Once the module is installed, visit 'administer >> content management >> autolink >>
   settings' to configure the module settings.
2. Global settings (Autolink Links module) are used during the link generation process
   to determine the amount and frequency of the generation of links.
3. Default settings are used to populate new link form data as well as during
   bulk generation of link definitions. These settings can ensure that link definitions
   that are generated through bulk operations will comply with your site's format.
4. Autolink modules provide support for Token replacement patterns. These patterns are
   available for users and nodes and can be configured at administer >> content management >>
   autolink >> settings. The practice of replacement patterns vary depending on which
   Autolink module is using them. For Autolink Links, token patterns are used for the
   'title' attribute of links that are generated. For the Autolink Tracking module,
   token replacement patterns are used to generate the links that are placed in sidebar
   blocks.


------------------------
     INSTRUCTIONS
------------------------
Once link generation has been enabled for a content type, any node of that type will
have links generated once saved new or updated.


 Create a new link group:
-------------------------
Link groups work similar to taxonomy vocabularies in that they group links together and
allow links within the groups to be applied by content type.
1. Navigate to administer >> content management >> autolink.
2. Select the tab labeled "Add link group".
3. Fill out the form information.
4. Select which content types to apply links within the link group to.
5. Click "Save".


 To perform bulk operations on existing link groups:
----------------------------------------------------------
1. Navigate to administer >> content management >> autolink.
2. Select the link groups you would like to edit.
3. Select the operation to perform from the operations select list.
4. Click 'Update'.


 Create new link generation rules:
-----------------------------------


 Via the Autolink administration forms:
----------------------------------------
1. Navigate to administer >> content management >> autolink
2. To create a new link click 'Add link'. If you are using addon modules, then click the
   'add' tab that corresponds to the link type you would like to create.
3. Select the type of link definition you would like to create from the dropdown labeled
   'Select a link type'.
4. In the 'Keyword' field, enter the keyword you would like to be converted into links.
   Note: the 'keyword' field can have several words and links will only be generated
   if words in a node body match the full keywords defined.
5. Depending on the link type being created, different fields will be displayed.
6. Select the content types you would like these links to be displayed on.
7. Optionally, you can enter further link attributes with which to style the link.
8. Click 'Save' to save the new link rule.


 To edit an existing link generation rule:
-------------------------------------------
1. Navigate to administer >> content management >> autolink
2. Click 'Edit' next to the link you would like to edit.
3. Make necessary changes to each field.
4. Click 'Save' to save the changes.


 To bulk generate or remove links from nodes:
----------------------------------------------
1. Navigate to administer >> content management >> content.
2. Select the nodes you want to generate links on by checking the associated checkbox.
3. In the dropdown labeled 'Update options', select the appropriate operation.
4. Click 'Update'.


 To convert node content to links:
-----------------------------------
1. Create new content or edit an existing node.
   Note: automatic link generation must be enabled for the content type.
2. Links will be automatically generated when the node is saved.


------------------------
     UNINSTALLTION
------------------------
1. Disable the module at administer >> build >> modules.
2. Uninstall the module at administer >> build >> modules >> uninstall.


=======================================================================================
||||||||||||||||||||||||||||||||||||  LINK TYPES  |||||||||||||||||||||||||||||||||||||
=======================================================================================
The Autolink module provides several link types for implementing various types
of links and features on your site. One of these link types is required to be able
to generate links on your site, as the Autolink module itself does not generate 
links automatically. These link types simply pass information to the Autolink API on
how to generate certain links. All global and default settings still apply to each of
these link types.


------------------------
      EXTERNAL URL
------------------------
Links to external URLs should be used any time an administrator needs to link to a site
like Google or Facebook. This link type requires a full URL (ex. http://www.facebook.com).
URL Links can only be defined from the link add/edit form.


------------------------
          NODE
------------------------
Node Link is the primary link type to interact with the Autolink API. Node links can be
defined through two different methods. Administrators can select node titles from a
list on the link add/edit form or entering an internal Drupal path, normal or aliased.


 To define new link definitions from the node edit form:
---------------------------------------------------------
   Note: you must have automatic link generation turned on for a given content type
   before you can access its settings on the node edit form. Defining links from the
   node edit form will generate links that point to the node being edited. So, if you
   are editing node/12 then links defined on that form will link to node/12. To provide
   default settings for these links, visit administer >> content management >> autolink >>
   settings. Links that are generated via this method will be assigned all active
   content types.

1. Navigate to the node edit form either by clicking the 'Edit' tab while viewing
   a node or by navigating to create content >> content type.
2. Open the fieldset titled 'Automatic link generation'.
3. You will be displayed a text field for each link group with this content type enabled.
4. If there are already link definitions that point to this node then they will be
   displayed in a comma separated list in the 'keywords' field.
5. To update the links for this node, enter a comma separated list of keywords you
   would like converted to links and pointed to this node.
   Note: if you remove keywords that were already defined for this node then they will
   be removed from the database.
6. Link attributes like style, class, and target can be defined in the default settings
   at 'administer >> content management >> autolink >> settings'.
7. Save the node.


 To bulk generate link definitions from node titles:
-----------------------------------------------------
Note: Links that are generated through this method will be assigned all active content types.
Default settings will be applied to generated links and can be defined on the settings form
at administer >> content management >> autolink >> settings.

1. Navigate to 'administer >> content management >> content'.
2. Select the nodes from which you want to generate link definitions.
3. In the dropdown labeled 'Update options', select 'Generate link definitions from titles'.
4. Click 'Update'.


------------------------
      USER PROFILE
------------------------
User profile links are created to link users to specific user pages. This link type provides
several methods of defining links. First, from the link add/edit form, administrators can
select from three different methods of defining a user for a link. Select a user name from
a dropdown list, enter a username, or enter a user's email address.


To define user profile links from the user admin overview page:
---------------------------------------------------------------
User profile links can also be created through user operations. To accomplish this,
navigate to administer >> user management >> users, select from the list of users which
users you would like to create links for, select 'Create link definitions from user names',
and submit the form. These link definitions will be created with the default attribute
settings at administer >> content management >> autolink >> settings.


------------------------
     TAXONOMY TERM
------------------------
Taxonomy links add the ability to link to taxonomy term pages with the Autolink
module. Terms can be selected either from a select list of term name or multiple
terms may be usedby entering terms in a comma separated list. Its link definitions
support multiple term linking with AND or OR operators. Only taxonomy terms that exist
can be used in link definitions. The taxonomy link type will not create new taxonomy
terms based on form input.


------------------------
 CONTENT SEARCH RESULTS
------------------------
Content search links provide the ability to define links that direct users to search
results pages. It adds elements to the 'Add link' form that resemble an advanced
search form. The advanced search form is optional, but search terms must be entered.


------------------------
  USER SEARCH RESULTS
------------------------
User search links function similar to content search results in that they simply generate
links that use the standard Drupal user search. Since user search in Drupal does not have
advanced parameters, this link type accepts only keywords to search for.


=======================================================================================
|||||||||||||||||||||||||||||||||  AUTOLINK TRACKING  |||||||||||||||||||||||||||||||||
=======================================================================================


------------------------
     CONFIGURATION
------------------------
1. Navigate to administer >> content management >> autolink >> settings.
2. Open the fieldset labeled "Autolink tracking and relationship settings".
3. Configure the intelligent link tracking and weights.
4. Click "Save configuration".


------------------------
  INTELLIGENT LINKING
------------------------
Intelligent linking is a process by which the Autolink Tracking module rates the relevance
of individual link definitions' keywords in relation to a given node. This process is
fully automated. To activate Intelligent linking follow the configuration steps above.


------------------------
         BLOCKS
------------------------
Autolink Tracking provides blocks that use link definitions to relate content to each other.
To enable 'Related content' blocks navigate to administer >> build >> blocks and enable
one of the Related Content blocks. If the 'Related links to content' block is enabled,
when a user is viewing a node, user, or taxonomy term, Autolink will display nodes
that link to that page. If the 'Related links from content' block is enabled, Autolink
will display links that have or would be generated on the current node.


=======================================================================================
||||||||||||||||||||||||||||||||  AUTOLINK LINKS FIELD  |||||||||||||||||||||||||||||||
=======================================================================================


------------------------
     CONFIGURATION
------------------------
1. Visit the content types administration page at administer >> content management >>
   content types.
2. Add the Autolink links field to your content type of choice.
3. In the field settings, enter the number of links to display.
4. Save the field.
