<?php

/**
 * @file
 * Hooks provided by the Autolink module.
 */

/**
 * Alter the $links array before a node is processed for links.
 *
 * @param $nid
 *   The node ID of the node being processed
 * @param $text
 *   The text that is being processed. This is either a full body or node teaser.
 * @param &$links
 *   An associative array of link objects to alter where the key is the link ID
 *   and value is a link object. This argument is passed by reference.
 */
function hook_autolink_preprocess_links($nid, $text, &$links) {

}

/**
 * Defines bulk operations for use on the link definition overview page.
 */
function hook_autolink_operations() {
  return array(
    'unblock' => array(
      'label' => t('Activate the selected link definitions'),
      'callback' => 'autolink_autolink_operations_activate',
    ),
    'block' => array(
      'label' => t('Disable the selected link definitions'),
      'callback' => 'autolink_autolink_operations_disable',
    ),
  );
}

/**
 * Defines filters for the link definition overview page.
 */
function hook_autolink_filters() {
  return array(
    'status' => array(
      'title' => t('status'),
      'where' => 'a.status = %d',
      'join' => '',
      'options' => array(1 => t('active'), 0 => t('disabled')),
    ),
  );
}

/**
 * Defines types of links that can be defined.
 */
function hook_autolink_link_types() {
  return array(
    'link' => t('Link to path'),
    'search' => t('Link to search results'),
    'taxonomy' => t('Link to taxonomy term'),
  );
}

/**
 * Defines form elements specific to a link type for use on the link add/edit form.
 *
 * Link type form elements should always include an 'argument' field, which is used
 * as the primary identifier of links on the link definition overview page. Additionally,
 * field names should correspond to database fields where possible as the module uses
 * drupal_write_record() to insert or update link data base on the database schema.
 *
 * @param $edit
 *   An array of link data from an existing link that is being edited.
 *   This data should be used to set default values if possible.
 */
function hook_autolink_LINK_TYPE_form_elements($edit) {
  $form['argument'] = array(
    '#type' => 'textfield',
    '#title' => t('Path'),
    '#default_value' => $edit['argument'],
  );
  return $form;
}

/**
 * Defines link form validation callbacks for link type form elements.
 *
 * @param $form_values
 *   An associative array of form values where field names are the key and
 *   values are the form values.
 */
function hook_autolink_LINK_TYPE_validate($form_values) {
  if (empty($form_values['argument'])) {
    form_set_error('argument', t('You must enter a path'));
  }
}

/**
 * Modifies link data before it is inserted into the database.
 *
 * hook_autolink_LINK_TYPE_alter allows other modules to alter the
 * form data of a link based on the link type defined by the external
 * module. Providing this  unique hook allows different types of links
 * and associated paths to be developed, opening possibilities for linking
 * to taxonomy, search, user, and other paths. Link types should be
 * defined in a value field named 'type' on the link add form.
 *
 * @param &$link
 *   An array containing the link data entered in an administrative form
 *   before save. The hook passes this form data on to other modules so
 *   they may use form data to determine the correct path or query if
 *   necessary. Path should be stored in $link['link_path']. Link paths
 *   are not built on the form so they must be build in a hook if the
 *   module defines a new link type. Paths can be either externam urls
 *   or internal Drupal paths, but Drupal paths that are aliases should
 *   be converted to normally paths before being stored in the database.
 *   Optional queries should be stored in $link['query']. This data will
 *   be used to process link paths when links are generated.
 *
 * @return
 *   The new form data after it has been altered by external modules.
 *   Link data is passed by reference.
 */
function hook_autolink_LINK_TYPE_alter(&$link) {
  if (drupal_lookup_path('source', $link['argument']) == FALSE) {
    $link['link_path'] = $link['argument'];
  }
  else {
    // Get the normal path if this path is an alias.
    $link['link_path'] = drupal_lookup_path('source', $link['argument']);
  }
  // Set the node ID.
  $args = explode('/', $link['link_path']);
  $link['object_id'] = $args[1];
}

/**
 * Notifies modules that a link definition was saved or updated.
 *
 * hook_autolink_save() is invoked after a link definition has been saved
 * to the database. This allows external modules to react to the save and
 * update their own database tables, which is done with the autolink_search
 * module.
 *
 * @param $link
 *   An associative array of form data that was saved.
 * @param $update
 *   A boolean value indicating whether the link definition was
 *   SAVED_NEW or SAVED_UPDATED.
 */
function hook_autolink_link_save($values, $update = FALSE) {

}

/**
 * Notifies modules that a link definition was deleted.
 *
 * This hook is invoked after the link has already been removed from the database
 * tables. A useful function for this hook is to use it to update another module's
 * own separate but related tables, as the autolink_search module does.
 *
 * @param $lid
 *   The link ID of the link definition that was deleted.
 */
function hook_autolink_link_delete($lid) {

}
