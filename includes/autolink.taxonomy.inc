<?php

/**
 * @file
 * Functions for the 'taxonomy' link type for use with the Autolink module.
 */

/**
 * Implementation of hook_autolink_LINK_TYPE_form_elements().
 */
function autolink_autolink_taxonomy_form_elements($edit) {
  if ($edit['method'] == 'dropdown') {
    if (isset($edit['link_path'])) {
      $args = explode('/', $edit['link_path']);
    }

    $terms = _autolink_taxonomy_get_terms();

    $select['select'] = t('--Select a term--');
    $select += $terms;

    $form['argument'] = array(
      '#type' => 'select',
      '#title' => t('Select a term'),
      '#required' => TRUE,
      '#options' => $select,
      '#default_value' => isset($args[2]) ? $args[2] : 'select',
    );
  }
  elseif ($edit['method'] == 'terms') {
    if (isset($edit['link_path']) && !empty($edit['link_path'])) {

      // Get the term names from tids by parsing the path.
      $args = explode('/', $edit['link_path']);
      $tids = explode(',', $args[2]);

      foreach ($tids as $tid) {
        $term = taxonomy_get_term($tid);
        $terms[$tid] = $term->name;
        asort($terms);
        $terms_default = implode(', ', $terms);
      }

      unset($tid);
    }

    $form['argument'] = array(
      '#type' => 'textfield',
      '#title' => t('Taxonomy term(s)'),
      '#required' => TRUE,
      '#default_value' => isset($terms_default) ? $terms_default : '',
      '#description' => t('Enter a comma separated list of taxonomy terms to which the anchor should link.'),
    );
    $form['operator'] = array(
      '#type' => 'radios',
      '#title' => t('Operation'),
      '#options' => array(
        'and' => 'and',
        'or' => 'or',
      ),
      '#default_value' => isset($edit['operator']) ? $edit['operator'] : '',
    );
  }

  return $form;
}

/**
 * Implementation of hook_autolink_LINK_TYPE_validate().
 */
function autolink_autolink_taxonomy_validate($form_values) {
  if ($form_values['method'] == 'dropdown' && $form_values['argument'] == 'select') {
    form_set_error('argument', t('You must select a term.'));
  }
  elseif ($form_values['method'] == 'terms') {
    $terms = drupal_explode_tags($form_values['argument']);

    // Ensure an operation is selected if multiple terms were entered.
    if (isset($terms[1]) && empty($form_values['operator'])) {
      form_set_error('operation', t('You must select an operation for multiple term definitions.'));
    }

    // Check to ensure terms exist.
    foreach ($terms as $term) {
      if (!autolink_taxonomy_term_exists($term)) {
        form_set_error('argument', t('Cannot locate term data for \'%term\'. Please enter another term.', array('%term' => $term)));
      }
    }
    unset($term);
  }
}

/**
 * Implementation of hook_autolink_LINK_TYPE_alter().
 *
 * Converts the link path to be saved to a taxonomy page. Form data is used to
 * create a proper link path linking to the results page.
 *
 * @param $link
 *   An associative array of link form data.
 *
 * @return
 *   A link array with new path which will be reference by created anchor tags.
 */
function autolink_autolink_taxonomy_alter(&$link) {
  if ($link['method'] == 'dropdown') {
    $link['link_path'] = 'taxonomy/term/'. $link['argument'];
  }
  elseif ($link['method'] == 'terms') {
    $terms = drupal_explode_tags($link['argument']);
  
    // Get term IDs for use in urls.
    foreach ($terms as $name) {
      $result = db_query("SELECT * FROM {term_data} WHERE name = '%s'", $name);
      while ($term = db_fetch_object($result)) {
        $tids[] = $term->tid;
      }
    }
  
    // If this is only one term then we don't need an operation.
    if (!isset($terms[1])) {
      $arg = $tids[0];
    }
    else {
      // The operation field is a special field created for filtering AND and OR statements.
      switch ($link['operator']) {
        case 'and':
          $arg = implode(',', $tids);
          break;
        case 'or':
          $arg = implode('+', $tids);
          break;
      }
    }
    $link['link_path'] = 'taxonomy/term/'. $arg;
  }

  // No extra query is needed for taxonomy so we just set the link path.
  $link['operation'] = $link['method'];

  // Set alias if necessary.
  if (variable_get('autolink_display_alias', 1) == AUTOLINK_SETTING_ENABLED && drupal_lookup_path('alias', $link['link_path']) != FALSE) {
    $link['argument'] = drupal_lookup_path('alias', $link['link_path']);
  }
  else {
    $link['argument'] = $link['link_path'];
  }
}

/**
 * Returns an array of all taxonomy terms.
 */
function _autolink_taxonomy_get_terms() {
  $result = db_query('SELECT * FROM {term_data}');
  while ($term = db_fetch_object($result)) {
    $terms[$term->tid] = $term->name;
  }
  asort($terms);
  return $terms;
}

/*
 * Helper function to determine whether a term exists.
 *
 * This function is used on taxonomy link form validation to ensure that the user is
 * entering terms that exist. The terms are taken from a comma separated list of keywords
 * and validated by querying the {term_date} table.
 *
 * @param $term
 *   A term to check for.
 *
 * @return
 *   TRUE if the term exists, FALSE otherwise.
 */
function autolink_taxonomy_term_exists($term) {
  $result = db_query("SELECT * FROM {term_data} WHERE name = '%s'", $term);
  return (db_fetch_object($result)) ? TRUE : FALSE;
}
