<?php

/**
 * @file
 * Provides filters for the groups overview page in Autolink administration.
 *
 * Filters:
 *   Content type.
 *   Status:
 *     -Active
 *     -Blocked
 */

/**
 * Build query for autolink admin filters based on session.
 */
function autolink_build_filter_query() {
  $filters = autolink_filters();

  // Build query.
  $where = $args = $join = array();
  foreach ($_SESSION['autolink_overview_filter'] as $filter) {
    list($key, $value) = $filter;
    $where[] = $filters[$key]['where'];
    $args[] = $value;
    $join[] = $filters[$key]['join'];
  }

  $where = !empty($where) ? 'WHERE '. implode(' AND ', $where) : '';
  $join = !empty($join) ? ' '. implode(' ', array_unique($join)) : '';

  return array('where' => $where, 'join' => $join, 'args' => $args);
}

/**
 * List autolink administration filters that can be applied.
 */
function autolink_filters() {
  foreach (module_invoke_all('autolink_filters') as $key => $filter) {
    $filters[$key] = $filter;
  }
  return $filters;
}

/**
 * Implementation of hook_autolink_filters().
 */
function autolink_autolink_filters() {
  // Node type filters.
  $filters = array();
  $node_types = node_get_types('names');
  if (count($node_types)) {
    $filters['node_type'] = array(
      'title' => t('content type'),
      'where' => "t.node_type = '%s'",
      'options' => $node_types,
      'join' => '',
    );
  }

  // Status filters.
  $filters['status'] = array(
    'title' => t('status'),
    'where' => 'g.status = %d',
    'join' => '',
    'options' => array(1 => t('active'), 0 => t('disabled')),
  );

  return $filters;
}
