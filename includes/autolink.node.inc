<?php

/**
 * @file
 * Functions for the 'link' link type for use with the Autolink module.
 */

/**
 * Save links from data sent from the node edit form.
 */
function autolink_node_save_links($node, $links) {
  if (isset($links['keywords'])) {
    $path = 'node/'. $node->nid;

    $typed_keywords = $links['keywords'];
    $keyword_defaults = $links['defaults'];
    unset($links['keywords'], $links['defaults']);

    // The user changed the links: we need to delete the old keywords and replace them with new ones.
    $node_links = array();
    $result = db_query("SELECT * FROM {autolink} WHERE link_path = '%s' ORDER BY keyword ASC", $path);
    while ($link = db_fetch_object($result)) {
      $node_links[$link->group_type][$link->lid] = $link->keyword;
    }

    foreach ($typed_keywords as $group => $group_keywords) {
      if ($group_keywords !== $keyword_defaults[$group]) {
        if (!is_array($node_links[$group])) {
          $node_links[$group] = array();
        }
        // Delete link definitions that were removed from the keywords list.
        foreach ($node_links[$group] as $lid => $keyword) {
          if (preg_match('/\b'. preg_quote($keyword) .'\b/i', $group_keywords) == 0) {
            autolink_link_delete($lid);
            drupal_set_message(t('Deleted link definition for keyword \'%keyword\'', array('%keyword' => $keyword)));
          }
        }

        // Finally populate the link definition array and save the new links.
        $keywords = drupal_explode_tags($group_keywords);
        foreach ($keywords as $keyword) {
          if (!autolink_link_exists($keyword, $path)) {
            autolink_node_save_link_by_nid($node->nid, $group, $keyword);
            drupal_set_message(t('Created new link definition for keyword \'%keyword\'', array('%keyword' => $keyword)));
          }
        }
      }
    }
  }
}

/**
 * Saves a new link definition based on the node ID of the node to which the link links.
 *
 * We populate all the link data based on the few parameters passed to the
 * function. This function is used during bulk operations, so we populate most
 * of the data with global and default settings that are established on the
 * Autolink settings form. We store the normal path in 'link_path' and get a
 * path alias if possible to store in the 'argument' field of the database.
 *
 * @param $nid
 *   A node ID.
 * @param $group
 *   A link group.
 * @param $keyword
 *   A keyword to store in the link definition.
 */
function autolink_node_save_link_by_nid($nid, $group, $keyword) {
  // Set the path variables based on normal and alias if available.
  $path = 'node/'. $nid;
  $alias = drupal_lookup_path('alias', $path);

  // Populate the link definition data with settings and parameters.
  $link['type'] = 'node';
  $link['group_type'] = $group;
  $link['keyword'] = $keyword;
  $link['argument'] = ($alias == FALSE) ? $path : $alias;
  $link['link_path'] = $path;
  $link['operation'] = 'path';
  $link['case_sensitive'] = variable_get('autolink_case_sensitivity', 0);

  drupal_write_record('autolink', $link);
}

/**
 * Delete all links that point to a recently deleted node's path.
 */
function autolink_delete_node_links($node) {
  // First get all the links that point to this node.
  $path = 'node/'. $node->nid;

  // We run a select query and delete link definitions individually to allow
  // other modules to respond to hook_autolink_link_delete().
  $links = array();
  $query = db_query("SELECT * FROM {autolink} WHERE link_path = '%s'", $path);
  while ($link = db_fetch_object($query)) {
    $links[$link->lid] = $link;
  }

  foreach ($links as $link) {
    autolink_link_delete($link->lid);
  }
}

/**
 * Implementation of hook_autolink_LINK_TYPE_form_elements().
 */
function autolink_autolink_node_form_elements($edit) {
  // Get the node ID from the link_path to set default values.
  if (isset($edit['link_path']) && !empty($edit['link_path'])) {
    $args = explode('/', $edit['link_path']);
    $nid = $args[1];
  }

  if ($edit['method'] == 'title') {

    $titles = autolink_node_get_titles();

    $select['select'] = t('--Select a node--');
    $select += $titles;

    $form['argument'] = array(
      '#type' => 'select',
      '#title' => t('Select a node title'),
      '#options' => $select,
      '#default_value' => isset($nid) ? $nid : NULL,
    );
  }
  elseif ($edit['method'] == 'path') {
    $form['argument'] = array(
      '#type' => 'textfield',
      '#title' => t('Enter a path'),
      '#required' => TRUE,
      '#default_value' => isset($edit['argument']) ? $edit['argument'] : '',
      '#description' => t('Provide a valid Drupal node path, normal or aliased.<br />' .
                          'Example: \'node/12\' or \'foo/bar\''),
    );
  }

  return $form;
}

/**
 * Implementation of hook_autolink_LINK_TYPE_validate().
 */
function autolink_autolink_node_validate($form_values) {
  if ($form_values['method'] == 'path') {
    $form_values['link_path'] = $form_values['argument'];
    // Validate the path as an internal aliased or normal Drupal path.
    if (!menu_valid_path($form_values) && !drupal_lookup_path('source', $form_values['argument']) && $form_values['argument'] !== 'contact') {
      form_set_error('argument', t('The path \'%path\' is invalid. You must provide a valid internal Drupal path.', array('%path' => $form_values['link_path'])));
    }
  }
  elseif ($form_values['method'] == 'title' && $form_values['argument'] == 'select') {
    form_set_error('argument', t('The node ID \'%nid\' is invalid. You must provide a valid node ID.', array('%nid' => $form_values['link_path'])));
  }
}

/**
 * Implementation of hook_autolink_LINK_TYPE_alter().
 *
 * Buils a value for the link_path field.
 *
 * @param $link
 *   An array of form data for the link definition.
 */
function autolink_autolink_node_alter(&$link) {
  if ($link['method'] == 'title') {
    $link['link_path'] = 'node/'. $link['argument'];
  }
  elseif ($link['method'] == 'path') {
    if (drupal_lookup_path('source', $link['argument']) == FALSE) {
      $link['link_path'] = $link['argument'];
    }
    else {
      $link['link_path'] = drupal_lookup_path('source', $link['argument']);
    }
  }

  if (variable_get('autolink_display_alias', 1) == AUTOLINK_SETTING_ENABLED && drupal_lookup_path('alias', $link['link_path']) != FALSE) {
    $link['argument'] = drupal_lookup_path('alias', $link['link_path']);
  }
  else {
    $link['argument'] = $link['link_path'];
  }

  $link['operation'] = $link['method'];
}

/**
 * Returns an array of node IDs and titles for the link add/edit form.
 */
function autolink_node_get_titles() {
  $result = db_query('SELECT n.nid, n.title FROM {node} n');
  while ($node = db_fetch_object($result)) {
    $nodes[$node->nid] = $node->title;
  }
  asort($nodes);
  return $nodes;
}
