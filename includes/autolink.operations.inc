<?php

/**
 * @file
 * Provides form filters and operations for the Autolink module.
 *
 * Operations:
 *   Disable link definitions.
 *   Activate link definitions.
 *   Delete link definitions.
 */

/**
 * Implementation of hook_autolink_operations().
 */
function autolink_group_operations($form_state = array()) {
  $operations = array(
    'activate' => array(
      'label' => t('Activate the selected link definitions'),
      'callback' => 'autolink_group_operations_activate',
    ),
    'disable' => array(
      'label' => t('Disable the selected link definitions'),
      'callback' => 'autolink_group_operations_disable',
    ),
    'delete' => array(
      'label' => t('Delete the selected link groups'),
    ),
  );

  $node_types = node_get_types('names');

  if ($node_types) {
    $add_node_types = array();
    foreach ($node_types as $key => $value) {
      $add_node_types['add_node_type-'. $key] = $value;
    }

    $remove_node_types = array();
    foreach ($node_types as $key => $value) {
      $remove_node_types['remove_node_type-'. $key] = $value;
    }
  }

  if (count($node_types)) {
    $node_type_operations = array(
      t('Add a content type to the selected link groups') => array(
        'label' => $add_node_types,
      ),
      t('Remove a content type from the selected link groups') => array(
        'label' => $remove_node_types,
      ),
    );

    $operations += $node_type_operations;
  }

  if (!empty($form_state['submitted'])) {
    $operation_type = explode('-', $form_state['values']['operation']);
    $operation = $operation_type[0];
    if ($operation == 'add_node_type' || $operation == 'remove_node_type') {
      $type = $operation_type[1];
      $operations[$form_state['values']['operation']] = array(
        'callback' => 'autolink_group_operations_node_type',
        'callback arguments' => array($operation, $type),
      );
    }
  }

  return $operations;
}

/**
 * Callback function for mass unblocking link definitions.
 */
function autolink_group_operations_activate($groups) {
  foreach ($groups as $type) {
    $group = autolink_group_load($type);
    // Skip activating group if it is already active.
    if ($group !== FALSE && $group->status == 0) {
      $group->status = 1;
      drupal_write_record('autolink_groups', $group, 'type');
    }
  }
}

/**
 * Callback function for mass blocking link definitions.
 */
function autolink_group_operations_disable($groups) {
  foreach ($groups as $type) {
    $group = autolink_group_load($type);
    // Skip disabling group if it is already disabled.
    if ($group !== FALSE && $group->status == 1) {
      $group->status = 0;
      drupal_write_record('autolink_groups', $group, 'type');
    }
  }
}

/**
 * Callback function for mass adding/deleting a content type.
 */
function autolink_group_operations_node_type($groups, $operation, $type) {
  $type_name = db_result(db_query("SELECT n.type, n.name FROM {node_type} n INNER JOIN {autolink_group_node_types} t ON n.type = t.node_type WHERE t.node_type = '%s'", $type));

  switch ($operation) {
    case 'add_node_type':
      foreach ($groups as $group_type) {
        $group = (array)autolink_group_load($group_type);
        // Skip adding the content types if the link definition already has it.
        if ($group !== FALSE && !isset($group['types'][$type])) {
          $types['node_types'] = is_array($group['types']) ? $group['types'] + array($type => $type_name) : $group['types'] = array($type => $type_name);
          $types['type'] = $group_type;
          autolink_group_save($group, $types);
        }
      }
      break;
    case 'remove_node_type':
      foreach ($groups as $group_type) {
        $group = (array)autolink_group_load($group_type);
        // Skip removing content types if the link definition already has it.
        if ($group !== FALSE && isset($group['types'][$type])) {
          $types['node_types'] = $group['types'];
          unset($types['node_types'][$type]);
          $types['type'] = $group_type;
          autolink_group_save($group, $types);
        }
      }
      break;
  }
}

/**
 * Delete confirmation form for link overview form.
 */
function autolink_group_multiple_delete_confirm(&$form_state) {
  $edit = $form_state['post'];

  $form['groups'] = array('#prefix' => '<ul>', '#suffix' => '</ul>', '#tree' => TRUE);
  // array_filter() returns only elements with TRUE values.
  foreach (array_filter($edit['groups']) as $type => $value) {
    $group = db_result(db_query("SELECT name FROM {autolink_groups} WHERE type = '%s'", $type));
    $form['groups'][$type] = array('#type' => 'hidden', '#value' => $type, '#prefix' => '<li>', '#suffix' => check_plain($group) ."</li>\n");
  }
  $form['operation'] = array('#type' => 'hidden', '#value' => 'delete');

  return confirm_form($form,
                      t('Are you sure you want to delete these link groups?'),
                      'admin/settings/autolink', t('This action cannot be undone.'),
                      t('Delete all'), t('Cancel'));
}

/**
 * Submit handler for deleting link definitions from the link overview form.
 */
function autolink_group_multiple_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    foreach ($form_state['values']['groups'] as $type => $value) {
      autolink_group_delete($type);
    }
    drupal_set_message(t('The link groups have been deleted.'));
  }
  $form_state['redirect'] = 'admin/content/autolink';
  return;
}

/**
 * Implementation of hook_autolink_operations().
 */
function autolink_link_operations($form_state = array()) {
  return array(
    'delete' => array(
      'label' => t('Delete the selected link definitions'),
    ),
  );
}

/**
 * Delete confirmation form for link overview form.
 */
function autolink_link_multiple_delete_confirm(&$form_state) {
  $edit = $form_state['post'];

  $form['links'] = array('#prefix' => '<ul>', '#suffix' => '</ul>', '#tree' => TRUE);
  // array_filter() returns only elements with TRUE values.
  foreach (array_filter($edit['links']) as $lid => $value) {
    $link = db_result(db_query('SELECT keyword FROM {autolink} WHERE lid = %d', $lid));
    $form['links'][$lid] = array('#type' => 'hidden', '#value' => $lid, '#prefix' => '<li>', '#suffix' => check_plain($link) ."</li>\n");
  }
  $form['operation'] = array('#type' => 'hidden', '#value' => 'delete');

  return confirm_form($form,
                      t('Are you sure you want to delete these link definitions?'),
                      'admin/settings/autolink', t('This action cannot be undone.'),
                      t('Delete all'), t('Cancel'));
}

/**
 * Submit handler for deleting link definitions from the link overview form.
 */
function autolink_link_multiple_delete_confirm_submit($form, &$form_state) {
  $group = $form_state['values']['group'];
  if ($form_state['values']['confirm']) {
    foreach ($form_state['values']['links'] as $lid => $value) {
      autolink_link_delete($lid);
    }
    drupal_set_message(t('The link definitions have been deleted.'));
  }
  $form_state['redirect'] = "admin/content/autolink/$group";
  return;
}
