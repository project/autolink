<?php

/**
 * @file
 * Functions for the 'node_search' link type for use with the Autolink module.
 */

/**
 * Implementation of hook_autolink_LINK_TYPE_form_elements().
 */
function autolink_autolink_node_search_form_elements($edit) {
  $form['argument'] = array(
    '#type' => 'textfield',
    '#title' => t('Search terms'),
    '#required' => TRUE,
    '#default_value' => isset($edit['argument']) ? $edit['argument'] : '',
    '#description' => t('The search terms whose results users should be directed upon clicking the link.'),
  );

  return $form;
}

/**
 * Implementation of hook_autolink_LINK_TYPE_alter().
 *
 * Converts the link path to be saved to a search results page. Form data is used to
 * create a proper link path linking to the results page.
 *
 * @param $link
 *   An associative array of link form data.
 *
 * @return
 *   A link array with data to build full urls which will be reference by created anchor tags.
 */
function autolink_autolink_node_search_alter(&$link) {
  $link['link_path'] = 'search/node';
  $link['query'] = $link['argument'];
}
