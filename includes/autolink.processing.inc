<?php

/**
 * @file
 * Handles node text processing for the Autolink Links module.
 */

/**
 * Wrapper function for processing a node for links.
 */
function autolink_process_node(&$node) {
  $links = autolink_get_links($node->type);

  // Add automatic links to the $links array if enabled.
  if (variable_get('autolink_auto_generate', 0) == AUTOLINK_SETTING_ENABLED && variable_get('autolink_auto_node_types_'. $node->type, 0) == AUTOLINK_SETTING_ENABLED) {
    $extras = autolink_get_auto_links();
    if (!empty($extras)) {
      $links = array_merge($links, $extras);
    }
  }

  // Invoke hook_autolink_preprocess_links with $links passed by reference.
  $hook = 'autolink_preprocess_links';
  foreach (module_implements($hook) as $module) {
    $function = $module .'_'. $hook;
    $function($node->nid, $node->body, $links);
  }

  if (isset($links)) {
    $node->body = _autolink_process_node_text($node->body, $links);
    $node->teaser = _autolink_process_node_text($node->teaser, $links);
  }
}

/**
 * Process text for links.
 *
 * This process has a certain order largely based on global settings. First, the
 * text is processed for any links that were placed by Autolink by looking for the
 * class="autolink" class. Then, each keyword is run through a loop. If a keyword
 * is found in the text, a link will be built if that setting is enabled and a count
 * of how many times it was found in the text will be added to the $count variable.
 * Keywords will then be ranked based on how many times they were found in the text.
 * That data will be used later to calculate overall relevance for all keywords.
 * When links are placed in content with preg_replace, we use the 'e' modifier to
 * call a using necessary paths, queries, and aliases in the autolink_build_link()
 * function. We use the $1 match variable to preserve letter casing.
 *
 * @param $text
 *   The text to be processed.
 * @param $links
 *   An associative array of link objects with metadata to use for link creation. Array
 *   is formatted with the link ID as key and link object as value.
 *
 * @return
 *   The processed text.
 *
 * @see autolink_process_node()
 */
function _autolink_process_node_text($text, $links) {
  // Get settings and prepare variables.
  $settings = autolink_get_settings('global');
  $defaults = autolink_get_settings('default');

  // Sets a limit on how many links should be generated based on global settings.
  $limit = ($settings->link_limit == 'all') ? -1 : $settings->link_limit;

  // Prepare the text for placing links.
  _autolink_preprocess_node_text($text, $links);

  foreach ($links as $link) {
    // Check if links need to be generated in the node text based on group settings.
    if ($link->in_content == AUTOLINK_SETTING_ENABLED) {
      $regex = $link->case_sensitive == AUTOLINK_SETTING_ENABLED ? '/(?!(?:[^<]+>|[^>]+<\/a>))\b('. preg_quote($link->keyword) .')\b/e' : '/(?!(?:[^<]+>|[^>]+<\/a>))\b('. preg_quote($link->keyword) .')\b/ie';
      $text = preg_replace($regex, 'autolink_build_link($link, $defaults, "$1")', $text, $limit);
    }
  }

  return $text;
}

/**
 * Prepare node text for link generation by removing existing Autolink links.
 *
 * Removes existing link tags only from Autolink links identified by the autolink class.
 * This is done to prevent unnecessary duplication of tags and thus invalid markup.
 * We check the anchor's plain text against each $link->keyword to make sure we don't
 * remove any tags that won't later be replaced.
 *
 * @param $text
 *   The node text.
 * @param $links
 *   The links that will be processed in the node text.
 *
 * @return
 *   The processed text with Autolink links removed.
 *
 * @see _autolink_process_node_text()
 */
function _autolink_preprocess_node_text(&$text, $links) {
  $html = new simple_html_dom();
  $html->load($text);

  if ($html->find('a[class^=autolink]')) {
    foreach ($html->find('a[class^=autolink]') as $element) {
      foreach ($links as $link) {
        if ($element->plaintext == $link->keyword) {
          $element->outertext = ''. $element->innertext .'';
        }
      }
    }
  }

  unset($element, $link);
  $text = $html->save();
}

/**
 * Build a link from a link object to be used as a replacement in text.
 *
 * @param $settings
 *   An object of default settings to apply to the link.
 * @param $link
 *   A link object with all its metadata.
 * @param $match
 *   The matched text from the haystack.
 *
 * @return
 *   A single link built from the attribute passed in parameters.
 *
 * @see _autolink_process_node_text()
 */
function autolink_build_link($link, $settings, $match) {
  // Set the target attribute based on whether the link is external.
  if ($link->type == 'link') {
    $target = $settings->external_target;
  }
  else {
    $target = $settings->internal_target;
  }

  // Build the path with data from the link object and url().
  // Internal paths should always be converted to real paths in the database so alias is FALSE.
  // We also build the link attributes in the $options array. They'll be used by l().
  $options = array(
    'query' => $link->query,
    'absolute' => TRUE,
    'alias' => FALSE,
    'attributes' => array(
      'title' => autolink_build_title($link),
      'style' => $settings->style,
      'class' => !empty($settings->class) ? 'autolink '. $settings->class : 'autolink',
      'target' => ($target !== 'none') ? $target : '',
      'rel' => $settings->external_rel == 1 && $link->type == 'external' ? 'nofollow' : '',
    ),
  );

  // Unset the empty or null attributes to avoid invalid markup.
  foreach ($options['attributes'] as $attribute => $value) {
    if (empty($value) || is_null($value)) {
      unset($options['attributes'][$attribute]);
    }
  }

  // The link() function will build the proper url using arguments in options.
  $anchor = l($match, $link->link_path, $options);
  return $anchor;
}

/**
 * Helper function to set a title attribute based on token patterns.
 *
 * @param $link
 *   A link object from which to generate the title.
 *
 * @return
 *   A link title with user or node token pattern replacements if necessary.
 *
 * @see autolink_build_link()
 */
function autolink_build_title($link) {
  $title = '';
  $args = explode('/', $link->link_path);

  if ($link->type == 'node') {
    $node = node_load($args[1]);
    $pattern = variable_get('autolink_node_title', '[title]');
    if (!empty($pattern)) {
      $title = token_replace($pattern, 'node', $node);
    }
    else {
      $title = $node->title;
    }
  }
  elseif ($link->type == 'user') {
    $user = user_load($args[1]);
    $pattern = variable_get('autolink_user_title', '[user]');
    if (!empty($pattern)) {
      $title = token_replace($pattern, 'user', $user);
    }
    else {
      $title = $user->name;
    }
  }
  else {
    // Set the link keyword as the title if no title can be generated.
    $title = $link->keyword;
  }

  return $title;
}

/**
 * Remove links in preparation for content editing.
 */
function autolink_unprocess_node(&$node) {
  $node->body = _autolink_preprocess_remove_links($node->body);
  $node->teaser = _autolink_preprocess_remove_links($node->teaser);
}

/**
 * Remove all links from a node body or teaser.
 *
 * Link removal can optionally be filtered by a keyword if one is supplied. Providing this
 * parameter ensures that only autolink links with that keyword will be removed.
 *
 * @param $text
 *   The text to process for links.
 * @param $keyword = NULL
 *   An optional keyword to match with a link for removal.
 *
 * @return
 *   The processed text with the appropriate links removed.
 *
 * @see autolink_unprocess_node()
 */
function _autolink_preprocess_remove_links($text, $keyword = NULL) {
  $html_obj = new simple_html_dom();
  $html_obj->load($text);

  // Get all links from the Autolink table and compare them against links in the node.
  if ($html_obj->find('a[class=autolink]')) {
    foreach ($html_obj->find('a[class=autolink]') as $element) {
      // Since we used the autolink class to find autolink links we can get rid of only anchors
      // with that class. This will help clean up deleted link definitions in the process.
      // First we check to see if we're only removing links with certain keywords.
      if (!is_null($keyword)) {
        if ($element->plaintext == $keyword) {
          $element->outertext = ''. $element->innertext .'';
        }
      }
      else {
        $element->outertext = ''. $element->innertext .'';
      }
    }
  }

  unset($element);
  $text = $html_obj->save();

  return $text;
}
