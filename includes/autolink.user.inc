<?php

/**
 * @file
 * Functions for the 'user' link type for use with the Autolink module.
 */

/**
 * Implementation of hook_autolink_LINK_TYPE_form_elements().
 */
function autolink_autolink_user_form_elements($edit) {
  if (isset($edit['link_path'])) {
    $args = explode('/', $edit['link_path']);
    $uid = $args[1];
    $user = is_numeric($uid) ? user_load(array('uid' => $uid)) : NULL;
  }

  if ($edit['method'] == 'dropdown') {

    $users = autolink_user_get_users();

    $select['select'] = t('--Select a user--');
    $select += $users;

    $form['argument'] = array(
      '#type' => 'select',
      '#title' => t('Users'),
      '#required' => TRUE,
      '#options' => $select,
      '#default_value' => isset($uid) ? $uid : 'select',
      '#description' => t('Links will link to the user profile of the selected user.'),
    );
  }
  elseif ($edit['method'] == 'name') {
    $form['argument'] = array(
      '#type' => 'textfield',
      '#title' => t('Enter a username'),
      '#required' => TRUE,
      '#default_value' => !is_null($user) ? $user->name : '',
    );
  }
  elseif ($edit['method'] == 'mail') {
    $form['argument'] = array(
      '#type' => 'textfield',
      '#title' => t('Enter a user email'),
      '#required' => TRUE,
      '#default_value' => !is_null($user) ? $user->mail : '',
    );
  }

  return $form;
}

/**
 * Implementation of hook_autolink_LINK_TYPE_validate().
 *
 * Handles validation for the 'user' link type add/edit form.
 */
function autolink_autolink_user_validate($form_values) {
  if ($form_values['method'] == 'dropdown' && $form_values['argument'] == 'select') {
    form_set_error('argument', t('You must select a user.'));
  }
  elseif ($form_values['method'] == 'name' && user_load(array('name' => $form_values['argument'])) == FALSE) {
    form_set_error('argument', t('The user \'%user\' does not exist.', array('%user' => $form_values['argument'])));
  }
  elseif ($form_values['method'] == 'mail' && user_load(array('mail' => $form_values['argument'])) == FALSE) {
    form_set_error('argument', t('Cannot locate user data for user email \'%email\'.', array('%email' => $form_values['argument'])));
  }
}

/**
 * Implementation of hook_autolink_LINK_TYPE_alter().
 *
 * Converts the link path to be saved to a search results page. Form data is used to
 * create a proper link path linking to the results page.
 *
 * @param $link
 *   An associative array of link form data.
 */
function autolink_autolink_user_alter(&$link) {
  if ($link['method'] == 'dropdown') {
    $link['link_path'] = 'user/'. $link['argument'];
  }
  elseif ($link['method'] == 'name' || $link['method'] == 'mail') {
    $user = user_load(array($link['method'] => $link['argument']));
    $link['link_path'] = 'user/'. $user->uid;
  }

  if (variable_get('autolink_display_alias', 1) == AUTOLINK_SETTING_ENABLED && drupal_lookup_path('alias', $link['link_path']) != FALSE) {
    $link['argument'] = drupal_lookup_path('alias', $link['link_path']);
  }
  else {
    $link['argument'] = $link['link_path'];
  }
  $link['operation'] = $link['method'];
}

/**
 * Saves a new link definition based on user data.
 */
function autolink_user_save_link($user, $group) {
  // Set path variables to aliases if available.
  $path = 'user/'. $user->uid;
  $alias = drupal_lookup_path('alias', $path);

  $link['type'] = 'user';
  $link['group_type'] = $group;
  $link['keyword'] = $user->name;
  $link['argument'] = ($alias == FALSE) ? $path : $alias;
  $link['link_path'] = $path;
  $link['operation'] = 'name';
  $link['case_sensitive'] = variable_get('autolink_case_sensitivity', 0);

  drupal_write_record('autolink', $link);
}

/**
 * Returns a list of users.
 */
function autolink_user_get_users() {
  $result = db_query('SELECT u.uid, u.name FROM {users} u WHERE u.uid <> 0');
  while ($user = db_fetch_object($result)) {
    $users[$user->uid] = $user->name;
  }
  asort($users);
  return $users;
}
