<?php

/**
 * @file
 * Functions for the 'link' link type for use with the Autolink module.
 */

/**
 * Implementation of hook_autolink_LINK_TYPE_form_elements().
 */
function autolink_autolink_link_form_elements($edit) {
  $form['argument'] = array(
    '#type' => 'textfield',
    '#title' => t('URL'),
    '#required' => TRUE,
    '#default_value' => isset($edit['argument']) ? $edit['argument'] : '',
    '#description' => t('e.g. \'http://www.google.com\''),
  );
  return $form;
}

/**
 * Implementation of hook_autolink_LINK_TYPE_validate().
 */
function autolink_autolink_link_validate($form_values) {
  // Validate the url as either an external path.
  if (!menu_path_is_external($form_values['argument'])) {
    form_set_error('argument', t('The url \'%url\' is invalid. You must provide a valid external url.', array('%url' => $form_values['argument'])));
  }
}

/**
 * Implementation of hook_autolink_LINK_TYPE_alter().
 *
 * Builds a value for the link_path field.
 *
 * @param $link
 *   An array of form data for the link definition.
 *
 * @return
 *   The modified path to be saved to the database.
 */
function autolink_autolink_link_alter(&$link) {
  $link['link_path'] = $link['argument'];
}
